using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public InputField namePlayer;

    public void LoadScene()
    {
        if(namePlayer.ToString() != "")
        {
            PlayerPrefs.SetString("Name", namePlayer.text);
            SceneManager.LoadScene("SampleScene");
        }  
    }

    public void MenuScene()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void PlayAgainScene()
    {
        SceneManager.LoadScene("SampleScene");
    }
}

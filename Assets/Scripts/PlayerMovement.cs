using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private SpriteRenderer sprite;
    private DataPlayer playerData;
    private Transform movement;
    private Rigidbody2D rb;
    private Animator animator;
    private float distance;
    public float movementDirection = 1;
    public Vector2 velocity;
    public float jumpVelocity = 5000;
    private float walk;
    public int isDead = 0;
   

    // Start is called before the first frame update
    void Start()
    {
        sprite = gameObject.GetComponent<SpriteRenderer>();
        playerData = gameObject.GetComponent<DataPlayer>();
        movement = gameObject.GetComponent<Transform>();
        distance = playerData.Distance;
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sprite.flipX = false;
    }

    // Update is called once per frame
    void Update()
    {
        walk = Input.GetAxisRaw("Horizontal");

        if (playerData.movementDependency == MovementDependency.Automatic)
        {
            animator.SetBool("IsWalking", true);
            PlayerAutomaticWalking();
        }
        else if (playerData.movementDependency == MovementDependency.Manual)
        {
            PlayerManualWalking();
            IsDead();
            //PlayerJump();
        }
        //IsDead();
    }

    private void PlayerManualMove()
    {
        rb.velocity = new Vector2(walk * playerData.Speed, rb.velocity.y);
    }
    private void PlayerAuomaticMove()
    {
        movement.position = new Vector3(movement.position.x + (movementDirection * playerData.Speed / playerData.Weight ), movement.position.y, movement.position.z);
    }

    private void PlayerManualWalking()
    {

        if (Input.GetAxis("Horizontal") > 0)
        {
            sprite.flipX = false;
            PlayerManualMove();
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            sprite.flipX = true;
            PlayerManualMove();
        }
        /*else if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(new Vector2(rb.velocity.x, jumpVelocity));
        }*/

        if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1f)
        {
            animator.SetBool("IsWalking", true);
            animator.SetBool("IsIdle", false);
        }
        else
        {
            animator.SetBool("IsWalking", false);
            animator.SetBool("IsIdle", true);
        }

    }

    private void PlayerJump()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(new Vector2(rb.velocity.x, jumpVelocity));
        }
    }

    private void PlayerAutomaticWalking()
    {
        PlayerAuomaticMove();
        distance -= playerData.Speed / playerData.Weight;
        
        if (distance < 0)
        {
            distance = playerData.Distance;
            movementDirection *= -1;
            sprite.flipX = !sprite.flipX;
        }
    }

    public void IsDead()
    {
        if (gameObject.transform.position.y < -5.5)
        {
            isDead = 1;
            PlayerPrefs.SetInt("Dead", isDead);
        }
    }
}

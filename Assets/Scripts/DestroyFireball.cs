using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyFireball : MonoBehaviour
{
    Animator animator;
    Rigidbody2D rb;
    DataPlayer player;
    public GameObject FireballPrefab;
    PlayerMovement playerMovement;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        playerMovement = GetComponent<PlayerMovement>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.tag == "Fireball")
        {
            animator.SetTrigger("HitSomething");
            rb.velocity = new Vector2 (0,0);
            rb.gravityScale = 0;
            //Explosi� que no mati al jugador 
            //animator.SetBool("CanKillPlayer", false);

            if (collision.tag == "Ground")
            {
                //animator.SetBool("CanKillPlayer", false);
                //collision.gameObject.GetComponent<DataPlayer>().SetFireball();
                //Instantiate(FireballPrefab);
                //Instantiate(FireballPrefab, transform.position, Quaternion.identity);
                //Vector3 newPos = new Vector3(Random.Range(minRandom, maxRandom), Random.Range(minRandom, maxRandom));
                //Instantiate(FireballPrefab, new Vector3(Mathf.Clamp(newPos.x, min, max), 5, 0), Quaternion.identity);
            }

            if (collision.tag == "Player")
            {
                Destroy(collision.gameObject);
                playerMovement.isDead = 1;
                PlayerPrefs.SetInt("Dead", playerMovement.isDead);
            }

            if (collision.tag == "Fireball")
            {
                Destroy(collision.gameObject);
            }

        }


    }
    public void OnExplotionAnimEnds()
    {
        Destroy(this.gameObject);
    }
}

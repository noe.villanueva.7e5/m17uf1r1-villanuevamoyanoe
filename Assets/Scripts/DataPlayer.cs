using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataPlayer : MonoBehaviour
{
    //private int fireball;

    /*public int GetFireball()
    {
        return fireball;
    }

    public void SetFireball()
    {
        fireball += 1;
    }


    public void SetFireball(int fb) //Override
    {
        fireball += fb;
    }*/

    public string PlayerName;
    public Sprite[] PlayerSprites;
    public Text nameText;
    public float Height;
    public float Weight;
    public float Speed;
    public float Distance;
    public MovementDependency movementDependency;
    public MovementState movementState;

    // Start is called before the first frame update
    void Start()
    {
        PlayerName = PlayerPrefs.GetString("Name");
        nameText.text = PlayerName;
    }
}
public enum MovementState
{
    Walking,
    OnGuard
}
public enum MovementDependency
{
    Automatic,
    Manual
}
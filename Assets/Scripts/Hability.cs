using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hability : MonoBehaviour
{
    private DataPlayer playerData;
    private float playerHeight;
    private Transform growingUp;
    private int habilityCooldown;
    public int habilityWaiting;
    public int habilityDuration;
    public HabilityModes modesHability;

    // Start is called before the first frame update
    void Start()
    {
        playerData = gameObject.GetComponent<DataPlayer>();
        growingUp = gameObject.GetComponent<Transform>();
        modesHability = HabilityModes.Avaliable;
        playerHeight = playerData.Height;
    }

    // Update is called once per frame
    void Update()
    {
        if (modesHability == HabilityModes.Avaliable) playerHeight = playerData.Height;
        else if (modesHability == HabilityModes.Growing)
        {
            UsingHability(0.5f);
            if (growingUp.localScale.x >= 15)
            {
                habilityCooldown = habilityDuration*15;
                modesHability = HabilityModes.Using;
            }
        }
        else if (modesHability == HabilityModes.Using)
        {
            habilityCooldown--;
            if (habilityCooldown == 0) modesHability = HabilityModes.Decreasing;
        }
        else if (modesHability == HabilityModes.Decreasing)
        {
            UsingHability(-0.5f);
            if (growingUp.localScale.x <= 7)
            {
                habilityCooldown = habilityWaiting*15;
                modesHability = HabilityModes.InCooldown;
            }
        }
        else if (modesHability == HabilityModes.InCooldown)
        {
            habilityCooldown--;
            if (habilityCooldown == 0) modesHability = HabilityModes.Avaliable;
        }
    }

    public void UsingHability(float increase)
    {
        playerData.Height += (playerHeight / 5) * increase;
        growingUp.localScale = new Vector3(growingUp.localScale.x + increase, growingUp.localScale.y + increase, growingUp.localScale.z);
    }
}

public enum HabilityModes
{
    Growing,
    Using,
    Decreasing,
    InCooldown,
    Avaliable
}
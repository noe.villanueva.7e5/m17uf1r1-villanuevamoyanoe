using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject fireball;
    public float minRandom, maxRandom;
    private float nextActionTime = 0;
    public float period = 0.5f;
    public GameObject Player;
    // Start is called before the first frame update
    void Start()
    {
        minRandom = transform.position.x - 7.5f;
        maxRandom = transform.position.x + 7.5f;
        Player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if(Player.GetComponent<DataPlayer>().movementDependency == MovementDependency.Manual)
        {
            nextActionTime += Time.deltaTime;
            if (nextActionTime > period)
            {
                nextActionTime = 0;
                SpawnFireball();
            }
        }
        else
        { 
            if (Input.GetMouseButtonDown(0))
            {
                Vector2 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                SpawnFireballAtClick(cursorPos);
            }
             
        }
        
    }

    public void SpawnFireball()
    {
        Instantiate(fireball, new Vector3(Random.Range(minRandom, maxRandom), transform.position.y, 0), Quaternion.identity);
    }

    public void SpawnFireballAtClick(Vector2 cursorPos)
    {
        Instantiate(fireball, new Vector3(cursorPos.x, transform.position.y,0), Quaternion.identity);
    }
}

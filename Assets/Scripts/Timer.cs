using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    public float TimeLeft;
    public bool TimerOn = false;
    public Text TimerTxt;
    public bool GameFinished = false;
    GameObject player;
    
    // Start is called before the first frame update
    void Start()
    {
        TimerOn = true;
        //player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        //var plm = player.GetComponent<PlayerMovement>();
        
        if (TimerOn)
        {
            if(TimeLeft > 0)
            {
                TimeLeft -= Time.deltaTime;
                updateTimer(TimeLeft);
            }
            else
            {
                Debug.Log("Time is UP!");
                //Determinar vict�ria aqu�
                GameFinished = true;
                TimeLeft = 0;
                TimerOn = false;
               
            }
        }

        if (GameFinished == true)
        {
            StartCoroutine(waiter());
        }
        /*else if (GameFinished == false && plm.isDead == 1)
        {
            StartCoroutine(waiter1());
        }*/
    }

    IEnumerator waiter()
    {
        //Wait for 2 seconds
        yield return new WaitForSeconds(2);

        SceneManager.LoadScene("WinGameScene");
    }

    IEnumerator waiter1()
    {
        //Wait for 2 seconds
        yield return new WaitForSeconds(2);

        SceneManager.LoadScene("GameOverScene");
    }
    void updateTimer(float currentTime)
    {
        currentTime += 1;

        float minutes = Mathf.FloorToInt(currentTime / 60);
        float seconds = Mathf.FloorToInt(currentTime % 60);

        TimerTxt.text = string.Format("{0:00} : {1:00}", minutes, seconds);
    }
}

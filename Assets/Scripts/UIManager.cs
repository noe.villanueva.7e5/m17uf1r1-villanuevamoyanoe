using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class UIManager : MonoBehaviour
{
    public Text Frames;
    //private int frameRate = 0;
    //public PlayerMovement playerMovement;
    public Hability hability;
    public DataPlayer playerData;
    public SpriteRenderer sprite;

    private void Awake()
    {
        //playerData = GameObject.Find("Player").GetComponent<DataPlayer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        //playerMovement = gameObject.GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        /*if (playerData.isDead)
        {
            resultsDefeat.SetActive(true);
            Debug.Log("GameOver");
        }*/
        //Frames.text = frameRate.ToString() + " FPS";
        //frameRate++;
    }

    public void ActiveHability()
    {
        if (hability.modesHability == HabilityModes.Avaliable) hability.modesHability = HabilityModes.Growing;
    }

    public void SetDependencyManual()
    {
        playerData.movementDependency = MovementDependency.Manual;
    }

    public void SetDependencyAuto()
    {
        playerData.movementDependency = MovementDependency.Automatic;
    }
}
